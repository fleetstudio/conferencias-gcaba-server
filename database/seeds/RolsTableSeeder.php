<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
        //
        //    /**
        //     * Run the database seeds.
        //     *
        //     * @return void
        //     */
    public function run()
    {

        Rol::create([
            'description' => 'Student'
        ]);

        Rol::create([
            'description' => 'Admin'
        ]);
    }
}

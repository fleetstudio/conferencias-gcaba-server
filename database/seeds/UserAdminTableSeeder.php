<?php

use Illuminate\Database\Seeder;
use App\User;

class UserAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrator',
            'email' => 'admin@gcaba.gob.ar',
            'password' => bcrypt('Admin1234$.GCABA'),
            'role_id' => 2, /* admin role */
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Conference;

class ConferenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 25; $i++) {
            Conference::create([
                'available_positions' => $faker->randomNumber(2),
                'date' => $faker->date("Y-m-d H:i:s", 'now') ,
                'description' => $faker->text(100) ,
                'name' => $faker->name ,
                'enabled' => $faker->boolean(60)
            ]);
        }
    }
}

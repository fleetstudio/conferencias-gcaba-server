<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// AUTH
Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/refresh', 'Auth\LoginController@refresh');
Route::post('/logout', 'Auth\LoginController@logout');

// USER
Route::get('/users', 'UserController@index');
Route::post('/users', 'UserController@store');
Route::get('/users/{user}', 'UserController@show');
Route::put('/users/{user}', 'UserController@update');
Route::delete('/users/{user}', 'UserController@destroy');

Route::get('/me', 'UserController@me');
Route::put('/me', 'UserController@updateProfile');
Route::put('/me/password', 'UserController@updatePassword');

// CONFERENCEy
Route::get('/conferences', 'ConferenceController@index');
Route::post('/conferences', 'ConferenceController@store');
Route::get('/conferences/{conference}', 'ConferenceController@show');
Route::get('/conferences/{conference}/restore', 'ConferenceController@restore');
Route::put('/conferences/{conference}', 'ConferenceController@update');
Route::delete('/conferences/{conference}', 'ConferenceController@destroy');

//CONFERENCE USER
Route::post('/conferences/{conference}/register', 'UserConferenceController@store');
Route::delete('/conferences/{conference}/register', 'UserConferenceController@destroy');
Route::delete('/conferences/{conference}/{user}/register', 'UserConferenceController@destroy_user_conf');
Route::get('/conferences/{conference}/users', 'UserConferenceController@show_users_conference');
Route::get('/me/conferences', 'UserConferenceController@show_conferences_user');



# Local setup

1. copy the .env.example file to a new .env file
2. edit the values of the database connection
```dotenv
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1.example.com
DB_PORT=5432
DB_DATABASE=laravel_db
DB_USERNAME=root_user
DB_PASSWORD=password
```

# Quick start

## Docker
`docker-compose up --build`

## Composer
1. `composer install`
2. `php artisan key:generate`
3. `php artisan serve`

# Migrations
1. run `php artisan migrate`
this cmd will be running all the `/database/migrations/*.php` files.

# Seed
1. run `php artisan db:seed`
this cmd will be running all the `/database/seeds/*.php` files.

# Passport
1. `php artisan passport:install`

# Create relic folder
mkdir relic

# Install packages
composer install

# copy the required relic files inside him
cp -r app relic
cp -r bootstrap relic
cp -r config relic
cp -r database relic
cp -r public relic
cp -r resources relic
cp -r routes relic
cp -r storage relic
cp -r tests relic
cp -r vendor relic
cp .env .rnd .styleci.yml artisan composer.json composer.lock phpunit.xml README.md server.php package.json yarn.lock webpack.mix.js relic



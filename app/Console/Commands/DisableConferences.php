<?php

namespace App\Console\Commands;

use App\Conference;
use Illuminate\Console\Command;

class DisableConferences extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'disable:conferences';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for disable the conferences out date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $conferences = Conference::all();
        foreach ($conferences as $conference){
            if($conference->date < now())
            {
                $conference->enabled = false;
                $conference->save();
            }
        }
    }
}

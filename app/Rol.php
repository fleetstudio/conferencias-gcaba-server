<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    // Seed proposal
    protected $fillable = [
        'description'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Conference extends Model
{
    //

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'available_positions', 'date', 'enabled', 'description', 'name', 'deleted_at'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}

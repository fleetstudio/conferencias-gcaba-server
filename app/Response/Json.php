<?php

namespace App\Response;

class Json
{
	public static function to($ok = null, $msg = null, $data = null, $code = 200)
	{
		return response()->json([
			'ok'   => $ok,
			'msg'  => $msg,
			'data' => $data,
		], $code);
	}
}
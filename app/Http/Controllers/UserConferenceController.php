<?php

namespace App\Http\Controllers;

use App\Conference;
use App\User;
use Illuminate\Http\Request;

class UserConferenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin')->only(['destroy_user_conf']);
    }



    public function store(Conference $conference)
    {
        $user = auth('api')->user();

        $count_conf =  Conference::findOrFail($conference->id)->users->count();
        if($conference->available_positions == $count_conf)
            return response()->json('This conference already has full places',400);
        if($conference->date < now() || $conference->enabled === false)
            return response()->json('This conference is out of date or disabled',400);
        if($user->conferences()->where('conference_id',$conference->id)->count() > 0)
            return response()->json('You already booked in this conference',400);
        else{
            $resp =  $user->conferences()->save($conference);
            return response()->json([
                'ok' => true,
                'msg' => 'Success',
                'data'=> $resp,
            ]);
        }
    }


    public function destroy(Conference $conference)
    {
        $user = auth('api')->user();
        $user->conferences()->detach($conference);
        return response()->json([
            'ok' => true,
            'msg' => 'Successfully removed',
            'data'=> null,
        ], 204);

    }

    public function destroy_user_conf(Conference $conference, User $user)
    {
        $user->conferences()->detach($conference);
        return response()->json([
            'ok' => true,
            'msg' => 'Successfully removed',
            'data'=> null,
        ], 204);

    }

    public function show_users_conference( $conference)
    {
        $users = Conference::find($conference)->users->all();
        return response()->json([
            'ok' => true,
            'msg' => 'Success',
            'data'=> $users,
        ]);
    }

    public function show_conferences_user()
    {
        $user = auth('api')->user();
        $conferences = $user->conferences()->get();
        return response()->json([
            'ok' => true,
            'msg' => 'Success',
            'data'=> $conferences,
        ]);
    }
}

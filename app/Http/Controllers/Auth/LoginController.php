<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

use App\Response\Json;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');

        $this ->middleware('auth:api')->except('login', 'refresh');
        $this->client = Client::find(2);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'bail|required|string|email',
            'password'   => 'bail|required|string|min:6|max:60',
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $params = [
            'grant_type'    => 'password',
            'client_id'     => $this->client->id,
            'client_secret' => $this->client->secret,
            'username'      => $request->email,
            'password'      => $request->password,
            'scope'         => '*',
        ];

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        $response = Route::dispatch($proxy)->getContent();
        $data = json_decode($response, true);

        if (isset($data['error']))
        {
            return Json::to(false, $data['message'], null, 400);
        }

        $user = User::where('email', $request->email)->first()->toArray();
        $data = array_merge($data, $user);
        return Json::to(true, 'Successful login', $data);
    }

    public function refresh(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'refresh_token' => 'required',
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $params = [
            'grant_type'    => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id'     => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope'         => '*',
        ];

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        $response = Route::dispatch($proxy)->getContent();
        $data = json_decode($response, true);

        if (isset($data['error']))
        {
            return Json::to(false, $data['message'], null, 400);
        }
        
        return Json::to(true, 'Successful refresh token', $data);
    }

    public function logout()
    {
        //$user_id = \Auth::user()->id;
        $user_id = auth('api')->user()->id;

        $tokens = DB::table('oauth_access_tokens')
            ->where('user_id', $user_id)->pluck('id');

        foreach ($tokens as $token)
        {
            DB::table('oauth_refresh_tokens')->where('access_token_id', $token)->delete();
        }
        DB::table('oauth_access_tokens')->where('user_id', $user_id)->delete();

        return Json::to(true, 'Logout successful', null);
    }


}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;

use App\Response\Json;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    private $client;

    private $key;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        $this->client = Client::find(2);
        $this->key = env('APP_KEY');
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'bail|required|string|email|unique:users',
            'password'   => ['bail', 'required', 'string', 'min:6', 'max:60', 'confirmed'],
            //'password'   => ['bail', 'required', 'string', 'min:6', 'max:60', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/', 'confirmed'],
            //'alias'      => 'bail|required|alpha_num|min:4|max:60|unique:users',
            //'first_name' => 'bail|required|alpha|min:2|max:25',
            //'last_name'  => 'bail|required|alpha|min:2|max:25',
            'name'  => 'bail|required|string|min:2|max:50',
            //'birthdate'  => ['bail', 'required', 'regex:/^((19\d{2})|(20\d{2}))-(((02)-(0[1-9]|[1-2][0-9]))|(((0(1|[3-9]))|(1[0-2]))-(0[1-9]|[1-2][0-9]|30))|((01|03|05|07|08|10|12)-(31)))$/', 'date'],
            //'gender'     => 'bail|required|boolean',
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $user = User::create([
            'email'      => $request->email,
            'password'   => bcrypt($request->password),
            'name'       => $request->name,
            'role_id'    => 1,
            //'alias'      => $request->alias,
            //'first_name' => $request->first_name,
            //'last_name'  => $request->last_name,
            //'birthdate'  => $request->birthdate,
            //'gender'     => $request->gender,
            //'tempword'   => $this->encrypt2($request->password),
        ]);

        $params = [
            'grant_type'    => 'password',
            'client_id'     => $this->client->id,
            'client_secret' => $this->client->secret,
            'username'      => $request->email,
            'password'      => $request->password,
            'scope'         => '*',
        ];

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        $response = Route::dispatch($proxy)->getContent();
        $data = json_decode($response, true);
        if (isset($data['error']))
        {
            return Json::to(false, $data['message'], null, 400);
        }

        $user = User::where('email', $request->email)->first()->toArray();
        $data = array_merge($data, $user);
        return Json::to(true, 'Successfully registered user', $data);
    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use App\Conference;
use Illuminate\Http\Request;

class ConferenceController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api')->except(['index', 'show']);
        $this->middleware('admin')->only(['store', 'update', 'restore', 'destroy', 'restore']);
    }

    public function index(Request $request)
    {
        $date_inic = $request['date_inic'];
        $date_end = $request['date_end'];
        if(is_null($date_inic) || is_null($date_end)) {
            $conferences = Conference::all();
            return response()->json([
                'ok' => true,
                'msg' => 'Success',
                'data'=> $conferences,
            ]);
        }
        else {
            if($request['skip'] === true)
                $conferences = Conference::whereBetween('date', [$date_inic, $date_end])->where('enabled',true)->get();
            else
                $conferences = Conference::whereBetween('date', [$date_inic, $date_end])->get();
            return response()->json([
                'ok' => true,
                'msg' => 'Success',
                'data'=> $conferences,
            ]);
        }

    }

    public function show(Conference $conference)
    {
        return response()->json([
            'ok' => true,
            'msg' => 'Success',
            'data'=> $conference,
        ]);
    }

    public function store(Request $request)
    {
        $resp = Conference::create($request->all());
        return response()->json($resp, 201);
    }

    public function update(Request $request, Conference $conference)
    {
        $conference->update($request->all());
        return response()->json($conference, 200);
    }

    public function destroy(Conference $conference)
    {
        $conference->enabled = false;
        $conference->deleted_at = now();
        $conference->save();
        return response()->json([
            'ok' => true,
            'msg' => 'Successfully removed',
            'data'=> null,
        ], 200);
    }

    public function restore(Conference $conference)
    {
        $temp = $conference;
        $conference->enabled = true;
        $conference->save();
        return response()->json([
            'ok' => true,
            'msg' => 'Successfully restored!',
            'data'=> $temp,
        ]);
    }

}

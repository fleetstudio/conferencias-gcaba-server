<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Response\Json;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin')->only(['index', 'store', 'update', 'destroy']);
    }

    public function index()
    {
        return Json::to(true, 'Success', User::where('role_id', 1)->get());
    }

    public function show(User $user)
    {
        return Json::to(true, 'Success', $user);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'bail|required|string|email|unique:users',
            'password'   => ['bail', 'required', 'string', 'min:6', 'max:60', 'confirmed'],
            'name'  => 'bail|required|string|min:2|max:50',
            'role_id'  => 'bail|required|integer|min:1'
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $user = User::create($request->all());
        return Json::to(true, 'Saved successfully!', $user, 201);
    }

    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'bail|required|string|email|unique:users,email,' . $user->id,
            'password'   => ['bail', 'required', 'string', 'min:6', 'max:60', 'confirmed'],
            'name'  => 'bail|required|string|min:2|max:50',
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $user->update($request->all());
        return Json::to(true, 'Update Successfully!', $user);
    }

    public function destroy(User $user)
    {
        $temp = $user;
        $user->delete();
        return Json::to(true, 'Successfully removed!', $temp, 200);
    }

    public function me()
    {
        $user = auth('api')->user();
        return Json::to(true, 'Success', $user);
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'bail|required|string|min:2|max:50',
            'avatar_url'  => 'string|min:4',
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $user = auth('api')->user();
        $user->update([
            'name' => $request->name,
            'avatar_url' => $request->avatar_url,
        ]);
        return Json::to(true, 'Successfully updated!', $user);
    }


    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password_old' => ['bail', 'required', 'string', 'min:6', 'max:60'],
            'password'     => ['bail', 'required', 'string', 'min:6', 'max:60', 'confirmed'],
        ]);

        if ($validator->fails())
        {
            $errors = $validator->errors();
            return Json::to(false, $errors->first(), null, 400);
        }

        $user = auth('api')->user();
        $password_old = $user->password;
        if (!Hash::check($request->password_old, $password_old))
        {
            return Json::to(false, 'Incorrect password!', null, 400);
        }

        $user->update([
            'password' => bcrypt($request->password),
        ]);
        return Json::to(true, 'Password changed!', $user);
    }
}

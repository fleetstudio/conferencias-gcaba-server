<?php

namespace App\Http\Middleware;

use Closure;
use App\Response\Json;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth('api')->user();
        if ($user &&  $user->role_id == 2) {
            return $next($request);
        }
        return Json::to(false, 'Permission denied!', null, 401);
    }
}
